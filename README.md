# Patchwork (Fast)API Gateway

Reusable API Gateway for Patchwork deployments. It's not directly tied to Patchwork framework, 
but in all regular use cases it's needed to integrate Patchwork nodes with some frontend.

This gateway exposes HTTP REST API using [FastAPI](https://fastapi.tiangolo.com/). All CUD actions
are translated to messages and send using given client to Patchwork Nodes. By default these tasks
are done asynchronously, so API returns 202 Accepted. If synchronous operation is needed,
Gateway adds standard callback hook to the message payload and awaits for it's invocation.

As Patchwork is designed for almost-fully asynchronous applications WebSockets support is
built-in to the Gateway and allows to forward (with optional transformation) messages from
internal queues to WS channels.

API Gateway internally handles authentication and authorization process with optional 2FA
support for whole API or given endpoints only. Permissions validation is also done here,
however API Gateway has no internal storage. Patchwork Node with users API and queues must
be setup.

For administration purposes there is an integrated HTTP panel to manage user sessions,
authorizations and permissions.

## Getting started

Create an application using `APIGateway` class from `patchwork.api_gateway` module. It's
extended FastAPI class with some preconfigurations and extensions for Patchwork. Then run
application using ASGI server, eg [Uvicorn](https://www.uvicorn.org/). 
 
Speed up building API with tools available in the package. Read more in docs.

## Compatibility

OS:
- any?

Python:
- 3.7
- 3.8

Patchwork:
- any client based on version 0.0.a4+