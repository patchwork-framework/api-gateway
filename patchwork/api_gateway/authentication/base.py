# -*- coding: utf-8 -*-

"""Created on 16.04.2020

.. moduleauthor:: Paweł Pecio
"""
from patchwork.core import Component


class AuthenticationBackend(Component):

    async def authenticate(self, userid: str, **credentials) -> bool:
        """
        Authenticate given userid with provided credentials.
        :param userid:
        :param credentials:
        :return: True if user has been authenticated, False otherwise
        """
        raise NotImplementedError()

    async def set_credentials(self, userid: str, **credentials) -> bool:
        """
        Sets provided credentials for given user.
        :param userid:
        :param credentials:
        :return: True if credentials has been set, False otherwise
        """
        raise NotImplementedError()

    async def reset_credentials(self, userid: str) -> bool:
        """
        Reset credentials for given user. Resetting means that user cannot authenticate at all
        and authenticate() method should always return False!
        :param userid:
        :return:
        """
        raise NotImplementedError()
