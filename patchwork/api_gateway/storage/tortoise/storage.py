# -*- coding: utf-8 -*-
from tortoise.query_utils import Q
from types import ModuleType

from datetime import timedelta

from tortoise.timezone import now

from patchwork.core.typing import ClassPath
from storage.tortoise import models

try:
    from tortoise import Tortoise, exceptions
except ImportError:
    raise Exception("Can't use Tortoise storage: tortoise-orm seems to be not installed")

from typing import Optional

from storage import Storage


class TortoiseStorage(Storage):

    class Config(Storage.Config):
        db_url: str
        table_name: str
        model: Optional[ClassPath]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        if self.settings.model is not None:
            # custom model, create fake module which reports only given model
            # as the only model, this avoids creating other models from imported module
            self._model = self.settings.model
            self._module = ModuleType("module")
            self._module.__models__ = [self._model]

        else:
            # no custom model, create fake module with dynamically created model
            # basing on user provided table name

            class KVModel(models.TortoiseKVModel):
                class Meta:
                    table = self.settings.table_name

            self._model = KVModel
            self._module = ModuleType("module")
            self._module.__models__ = [self._model]

    async def _start(self) -> bool:
        await Tortoise.init(db_url=self.settings.db_url, modules={
            'storage': [self._module]
        })

        await Tortoise.generate_schemas(safe=True)
        return True

    async def _stop(self) -> bool:
        # clear all expired entries
        await self._model.filter(expires__lte=now())
        await Tortoise.close_connections()
        return True

    async def _get(self, key: str) -> bytes:
        try:
            item = await self._model.get(key=key)
        except exceptions.DoesNotExist:
            raise LookupError(f'no such key: {key}')
        except exceptions.MultipleObjectsReturned:
            raise LookupError(f'multiple entries for key: {key}')

        if item.expires is not None and item.expires < now():
            raise LookupError(f'expired key: {key}')

        return item.value

    async def _set(self, key: str, data: bytes, ttl: Optional[int]):
        item = self._model(
            key=key,
            value=data,
            expires=now() + timedelta(seconds=ttl) if ttl is not None else None
        )
        await item.save()

    async def _exists(self, key: str) -> bool:
        return await self._model.exists(Q(expires__gte=now()) | Q(expires=None), key=key)

    async def _remove(self, key: str) -> bool:
        return await self._model.filter(key=key).delete() > 0
