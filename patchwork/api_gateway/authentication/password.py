# -*- coding: utf-8 -*-

"""Created on 16.04.2020

.. moduleauthor:: Paweł Pecio
"""
import base64
import hashlib
import logging
import secrets
from enum import Enum

from authentication.base import AuthenticationBackend
from patchwork.core.config.base import Dependency
from storage import Storage

logger = logging.getLogger("authentication.password")


class PwdAlgorithms(str, Enum):
    PBKDF2_HMAC = "pbkdf2_hmac"


class HashFunc(str, Enum):
    SHA512 = "sha512"


class PasswordAuthenticator(AuthenticationBackend):

    class Config(AuthenticationBackend.Config):
        algorithm: PwdAlgorithms = PwdAlgorithms.PBKDF2_HMAC
        hashing: HashFunc = HashFunc.SHA512
        iterations: int = 100000

        storage: Dependency[Storage]

    storage: Storage

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.storage = self.settings.storage.instantiate(parent=self)

    async def _start(self):
        return await self.storage.run()

    async def _stop(self):
        return await self.storage.terminate()

    async def authenticate(self, userid: str, password: str) -> bool:
        try:
            stored_pwd: bytes = await self.storage.get(userid)
        except LookupError:
            # no credentials
            return False

        if stored_pwd == b"?":
            # unusable password
            return False

        password = password.encode('latin-1')

        alg, hashfn, salt, iterations, result = stored_pwd.split(b"$")
        alg = alg.decode()
        hashfn = hashfn.decode()
        iterations = int.from_bytes(iterations, 'big')

        if alg == PwdAlgorithms.PBKDF2_HMAC:
            return self._compare_pbkdf2_hmac(password, hashfn, salt, iterations, result)

        logger.error(f"unsupported password algorithm '{alg}'")
        return False

    async def set_credentials(self, userid: str, password: str) -> bool:

        if self.settings.algorithm == PwdAlgorithms.PBKDF2_HMAC:
            salt = secrets.token_bytes(32)
            result = base64.b64encode(hashlib.pbkdf2_hmac(
                self.settings.hashing,
                password.encode("latin-1"),
                salt,
                self.settings.iterations
            ))

            data = b"$".join(
                (
                    str(self.settings.algorithm).encode(),
                    str(self.settings.hashing).encode(),
                    base64.b64encode(salt),
                    self.settings.iterations.to_bytes(4, 'big'),
                    result
                )
            )
        else:
            raise NotImplementedError(f"{self.settings.algorithm} is not implemented")

        await self.settings.store(data, userid)
        return True

    async def reset_credentials(self, userid: str) -> bool:
        await self.settings.store(b"?", userid)
        return True

    def _compare_pbkdf2_hmac(self, password: bytes, hashfn: str, salt: bytes, iterations: int, result: bytes):
        """
        Compare given password against stored one, using hash function and salt.
        :param password:
        :param hashfn:
        :param salt:
        :param result:
        :return:
        """
        try:
            our_result = hashlib.pbkdf2_hmac(
                hashfn,
                password,
                salt,
                iterations
            )
        except Exception as e:
            logger.error(f"can't validate password: {e.__class__.__name__}({e})")
            return False

        return secrets.compare_digest(our_result, result)
