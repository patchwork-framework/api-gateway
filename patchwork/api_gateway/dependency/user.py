# -*- coding: utf-8 -*-

"""Created on 07.04.2020

.. moduleauthor:: Paweł Pecio
"""
import logging

from fastapi import HTTPException, Depends
from starlette import status
from starlette.requests import Request

from middleware.session import Session

logger = logging.getLogger('gateway')


async def current_session(request: Request):
    await request.session.load()
    return request.session


async def get_current_userid(
    request: Request
) -> str:
    session: Session = request.session

    if not session.is_authenticated():
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
            headers={
                'WWW-Authenticate': session.userid
            }
        )

    return request.session.userid

"""



async def get_current_user(
    userid: str = Depends(get_current_userid)
) -> User:
    try:
        ret = await user_backend.get(userid)

        # make sure that backend is not returning None for not existing user, which is common mistake
        assert ret is not None

        return ret
    except LookupError:
        logger.exception(f"missing user instance for '{userid}'")
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR
        )
"""
# /login, must have: userid, password, verify password and on success sends user.logged_in message with userid,
#         optional 2FA seats between verifying password and login success message

# /logout, if there was active session terminates it, sends user.logged_out message with userid,
#          optional 'all' flag in query params allows to terminate all active sessions

# /reset_password, may have: userid; get current session or given one, check if exists in secrets and if so,
#                  sends pwd.reset message with userid and unique token

# /change_password, must have: userid, [old_password|token], new_password, if token/old_password matches,
#                   change password to new one, sends pwd.changed message

# /delete, must have: userid, removes stored credentials for given user, sends user.delete message with userid
