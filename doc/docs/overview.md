# API Gateway for Patchwork Framework

This module integrates [FastAPI](https://fastapi.tiangolo.com/) running on [Uvicorn](https://www.uvicorn.org/)  
into [Patchwork Node](https://patchwork-framework.gitlab.io/core) to add HTTP API functionality.
Currently Uvicorn has been tested only, however other ASGI servers compatible with FastAPI should work.

API Gateway has some ready to use functionality, however it should be considered as a bootstrap project to build 
a HTTP interface for microservices in Patchwork. According to Patchwork philosophy, none of internal service
should expose HTTP interface externally. Moreover, no synchronous actions are permitted, so this Gateway
is required to convert synchronous calls from web clients into asynchronous Patchwork world.

## Functionality
1) authentication

   Built-in authentication backends:

   - user/password
   - Google OAuth2 (in progress)

2) authorization

3) websockets channel to forward messages from Patchwork queues (planned)

4) synchronous to asynchronous conversion helper (planned, convert HTTP request to Patchwork Task, await for result notification and send appropriate response)

5) permissions management (to be discussed)

6) statistics (planned)

7) API documentation provider (on top of FastAPI)