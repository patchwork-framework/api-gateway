# -*- coding: utf-8 -*-

"""Created on 07.04.2020

.. moduleauthor:: Paweł Pecio
"""
from dependency._pool import DependencyPool


class AuthenticationPool(DependencyPool):

    def set_backends(self, backend_settings):
        self._set(backend_settings)

    def all(self):
        return self._mods


AuthBackends = AuthenticationPool()
