# -*- coding: utf-8 -*-

"""Created on 17.04.2020

.. moduleauthor:: Paweł Pecio
"""
from typing import Optional

from pydantic import BaseModel
from starlette.requests import HTTPConnection
from starlette.responses import JSONResponse
from starlette.types import Scope, Receive, Send

from middleware.session import Session


class User(BaseModel):

    userid: Optional[str]
    is_authenticated: bool

    def __str__(self):
        return f"{self.userid if self.userid else '<anonymous>'} " \
               f"({'authenticated' if self.is_authenticated else 'unauthenticated'})"


class UserMiddleware:
    def __init__(self, app):
        self.app = app

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        if scope["type"] not in ("http", "websocket"):  # pragma: no cover
            await self.app(scope, receive, send)
            return

        if "session" not in scope:
            # no active session
            scope["user"] = User(userid=None, is_authenticated=False)
            await self.app(scope, receive, send)
            return

        session: Session = scope["session"]

        connection = HTTPConnection(scope)
        auth_hdr = connection.headers.get("Authorization")

        if auth_hdr is None:
            # no auth header, request for current user if only one is in the session
            userid = session.current_userid
        else:
            auth_type, userid = auth_hdr.split(' ')
            if auth_type != 'session':
                response = JSONResponse({
                    "detail": "Invalid authorization type."
                }, status_code=401)
                await response(scope, receive, send)
                return

        if userid != session.current_userid:
            response = JSONResponse({
                "detail": "Request is not matching current session active user"
            }, status_code=401)
            await response(scope, receive, send)
            return

        scope["user"] = User(userid=userid, is_authenticated=session.is_authenticated(userid))

        await self.app(scope, receive, send)
