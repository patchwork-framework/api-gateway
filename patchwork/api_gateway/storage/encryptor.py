# -*- coding: utf-8 -*-

"""Created on 16.04.2020

.. moduleauthor:: Paweł Pecio
"""
import base64

from cryptography.fernet import Fernet

from patchwork.core.config import ImproperlyConfigured


class ClientEncryptor:

    def encrypt(self, data: bytes):
        """
        Encrypts given data
        :param data:
        :return:
        """
        raise NotImplementedError()

    def decrypt(self, data: bytes):
        """
        Decrypts given data
        :param data:
        :return:
        """
        raise NotImplementedError()


class PlainEncryptor(ClientEncryptor):

    def encrypt(self, data: bytes):
        return data

    def decrypt(self, data: bytes):
        return data


class FernetEncryptor(ClientEncryptor):

    def __init__(self, **opts):
        if 'secret_key' not in opts:
            raise ImproperlyConfigured(f"{self.__class__.__name__} requires `secret_key` to be configured")

        # secret key is URL-safe base64 encoded random bytes with removed padding
        base64_secret = opts['secret_key'].encode('ascii')

        if len(base64_secret) < 44:
            raise ImproperlyConfigured("Secret key is too short")

        # as a key use first 43 base64 encoded secret which encodes 32 bytes, add one padding char to align
        self._f = Fernet(base64_secret[:43] + b'=')

    def encrypt(self, data: bytes):
        # as encode returns Fernet token url-safe base64 encoded, decode it to save space on backend
        # most backends can store any binary data or if not, should encoded them in their way
        return base64.urlsafe_b64decode(self._f.encrypt(data))

    def decrypt(self, data: bytes):
        data = base64.urlsafe_b64encode(data)
        return self._f.decrypt(data)
