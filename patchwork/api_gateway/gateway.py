# -*- coding: utf-8 -*-

import asyncio
import logging
from typing import Tuple

from fastapi import FastAPI
from starlette.middleware import Middleware

from dependency import PatchworkPublishers
from dependency.auth import AuthBackends
from middleware import SessionMiddleware
from settings import GatewaySettings

logger = logging.getLogger("gateway")

settings: GatewaySettings


def get_settings():
    return settings


class Gateway(FastAPI):

    def __init__(self, settings_schema=GatewaySettings, **kwargs):
        global settings
        settings = settings_schema()

        PatchworkPublishers.set_clients(settings.publishers)
        AuthBackends.set_backends(settings.auth_backends)
        self._modules = [
            PatchworkPublishers,
            AuthBackends
        ]

        kwargs['middleware'] = self.get_gateway_middleware() + tuple(kwargs.get('middleware', []))
        kwargs['on_startup'] = (self._on_gateway_startup, ) + tuple(kwargs.get('on_startup', []))
        kwargs['on_shutdown'] = (self._on_gateway_shutdown,) + tuple(kwargs.get('on_shutdown', []))

        super().__init__(**kwargs)

    def get_gateway_middleware(self) -> Tuple[Middleware]:
        session_backend = settings.session.storage.instantiate(parent=self)
        self._modules.append(session_backend)

        return Middleware(
                SessionMiddleware,
                backend=session_backend,
                session_cookie=settings.session.cookie_name,
                max_age=settings.session.max_age,
                same_site=settings.session.same_site,
                https_only=settings.session.https_only
            ),

    async def _on_gateway_startup(self):
        jobs = []
        for mod in self._modules:
            job = asyncio.create_task(mod.run())
            job._module = mod
            jobs.append(job)

        if jobs:
            done, _ = await asyncio.wait(jobs)
            for fut in done:
                exc = fut.exception()
                if exc:
                    logger.error(f"can't start gateway module `{fut._module}`: {exc.__class__.__name__}({exc})", exc_info=exc)

        self.register_default_paths()

    async def _on_gateway_shutdown(self):
        jobs = []

        for mod in self._modules:
            jobs.append(asyncio.create_task(mod.terminate()))

        if jobs:
            done, _ = await asyncio.wait(jobs)
            for fut in done:
                exc = fut.exception()
                if exc:
                    logger.error(f"can't start gateway module: {exc.__class__.__name__}({exc})", exc_info=exc)

    def register_default_paths(self):
        from views.authorization import router as auth_router
        self.include_router(auth_router, prefix='/auth')
