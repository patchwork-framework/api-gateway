# -*- coding: utf-8 -*-

"""Created on 16.04.2020

.. moduleauthor:: Paweł Pecio
"""
from fastapi import APIRouter, Depends, HTTPException
from pydantic import BaseModel
from starlette import status

from authentication.base import AuthenticationBackend
from dependency.auth import AuthBackends
from dependency.user import current_session
from gateway import get_settings
from middleware.session import Session
from patchwork.api_gateway import GatewaySettings

router = APIRouter()


class AuthCredentials(BaseModel):
    userid: str
    credentials: dict


@router.get('/login', description="Returns authentication requirements")
async def login(settings: GatewaySettings = Depends(get_settings)):
    return list(settings.required_auth)


@router.post('/do/{backend_id}', status_code=status.HTTP_204_NO_CONTENT,
             description="Do authorization against given backend",
             responses={
                 401: {"description": "Invalid credentials"},
                 404: {"description": "No such authorization backend"}
             })
async def authenticate(
        data: AuthCredentials,
        backend_id: str,
        session: Session = Depends(current_session)
):
    if backend_id not in AuthBackends:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No such authorization backend")

    backend: AuthenticationBackend = await AuthBackends.get(backend_id)

    try:
        if await backend.authenticate(data.userid, **data.credentials):
            session.set_authenticated(data.userid, backend_id)
            return
    except TypeError:
        # invalid credential arguments
        pass

    raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Authentication failed")


@router.post('/logout', status_code=status.HTTP_204_NO_CONTENT,
             description="Remove all authentication information from current session")
async def logout(session: Session = Depends(current_session)):
    session.reset_authentication()
