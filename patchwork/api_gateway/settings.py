# -*- coding: utf-8 -*-

"""Created on 07.04.2020

.. moduleauthor:: Paweł Pecio
"""
from enum import Enum
from secrets import token_urlsafe
from typing import List, Union

from pydantic import BaseModel

from authentication.base import AuthenticationBackend
from patchwork.core.config.base import Dependency, DependencyMap, PatchworkConfig
from storage import Storage


class SameSiteEnum(str, Enum):
    strict = 'strict'
    lax = 'lax'
    none = 'none'


class SndFactorState(str, Enum):
    disabled = 'disabled'   # user can't set and use 2nd factor auth
    optional = 'optional'   # user can set and use 2nd if wish, but is not prompted to do that
    recommended = 'recommended'     # user can set and use 2nd and is prompted to configure and use
    forced = 'forced'       # user is forced to use 2nd factor and can't login without this


class SessionSettings(BaseModel):

    storage: Dependency[Storage]
    cookie_name: str = 'session_id'
    max_age: int = 14 * 24 * 60 * 60  # 14 days, in seconds
    same_site: SameSiteEnum = SameSiteEnum.lax
    https_only: bool = True


class GatewaySettings(PatchworkConfig):

    secret_key: str = token_urlsafe(64)

    publishers: DependencyMap = {}
    session: SessionSettings = {
        "storage": {
            "engine": "storage:LocMemStorage",
        }
    }

    # enable HTML response if browser sends Accept: text/html? by default disabled,
    # activating this option may be good for debug or testing when there is no frontend application done
    # note: exposed HTML interface is very simple, might not be safe, DO NOT USE it on production
    enable_html: bool = False

    # enable HTML admin interface to view and manage gateway settings?
    enable_admin: bool = False

    # id of superuser who is can use admin interface, this user does not have to exist on application startup
    # and might be registered later
    superuser_id: str = None

    # list of authentication backends enabled for the application
    auth_backends: DependencyMap[AuthenticationBackend] = {
        "password": {
            "engine": "authentication:PasswordAuthenticator",
            "options": {
                "storage": {
                    "engine": "storage:LocMemStorage",
                    "encryptor": {
                    }
                },
                "algorithm": "pbkdf2_hmac",
                "hashing": "sha512",
                "iterations": 100000
            }
        }
    }

    # list of authentication backend names against user must authenticate
    # if item is a list it means that user must authenticate against one of given backend
    # Example:
    #       To force user to authenticate against "password" and "sms_token" backends use:
    #       ["password", "sms_token"]
    #
    #       To force user to authenticate against "password" and ("sms_token" or "otp_device") use:
    #       ["password", ["sms_token", "otp_device"]]
    required_auth: List[Union[str, List[str]]] = ["password"]
