# -*- coding: utf-8 -*-
import os

import pytest

from storage.tortoise.storage import TortoiseStorage


@pytest.fixture
async def tortoise_storage():
    db_url = os.environ.get("TORTOISE_TEST_DB", "sqlite://:memory:")
    storage = TortoiseStorage(db_url=db_url, table_name='kvstorage')

    await storage.run()
    yield storage
    await storage.terminate()


@pytest.mark.asyncio
async def test_running(tortoise_storage):
    assert tortoise_storage.is_running


@pytest.mark.asyncio
async def test_store_get(tortoise_storage):
    assert await tortoise_storage.exists('test-key') is False

    await tortoise_storage.store("test-data", key='test-key')
    assert await tortoise_storage.exists('test-key')

    data = await tortoise_storage.get('test-key')
    assert data == "test-data"


@pytest.mark.asyncio
async def test_ttl(tortoise_storage):
    await tortoise_storage.store("test-data", key='test-key', ttl=0)
    assert await tortoise_storage.exists('test-key') is False


@pytest.mark.asyncio
async def test_remove(tortoise_storage):
    await tortoise_storage.store("test-data", key='test-key')
    assert await tortoise_storage.exists('test-key')

    await tortoise_storage.remove('test-key')
    assert await tortoise_storage.exists('test-key') is False
