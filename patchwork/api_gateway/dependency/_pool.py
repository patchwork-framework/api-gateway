# -*- coding: utf-8 -*-

"""Created on 16.04.2020

.. moduleauthor:: Paweł Pecio
"""
import asyncio
import logging
from typing import Dict, Any


logger = logging.getLogger('dependency')


class DependencyPool:
    """
    Abstract FastAPI dependency which returns instances of external components which starts
    at application start.
    """

    _mods: Dict[str, Any]

    def __init__(self):
        self._is_running = False
        self._running_event = asyncio.Event()

    def _set(self, mod_settings):
        self._mods = {k: v.instantiate() for k, v in mod_settings.items()}

    async def run(self):
        if not self._mods:
            logger.warning("No modules to start")
            return

        start_tasks = []
        for module in self._mods.values():
            if getattr(module, "is_running", False):
                logger.warning(f"Can't start already running module '{module}'")
                continue

            logger.debug(f"Starting module '{module}'")
            start_tasks.append(asyncio.create_task(module.run()))

        await asyncio.wait(start_tasks)
        logger.debug("All modules run() tasks completed")
        self._is_running = True
        self._running_event.set()

    async def terminate(self):
        self._is_running = False
        self._running_event.clear()

        term_tasks = []
        for module in self._mods.values():
            if not getattr(module, "is_running", True):
                logger.debug(f"Module '{module}' can't be stopped as is not running")
                continue

            logger.debug(f"Stopping module '{module}'")
            term_tasks.append(asyncio.create_task(module.terminate()))

        if term_tasks:
            await asyncio.wait(term_tasks)
        logger.debug("All modules terminate() tasks completed")

    async def get(self, mod_name: str):
        if mod_name not in self._mods:
            # early fail
            raise ValueError(f"no such module '{mod_name}'")

        await self._running_event.wait()
        return self._mods[mod_name]

    def __contains__(self, item):
        return item in self._mods

    def __call__(self, mod_name: str):
        return self.get(mod_name)

