# -*- coding: utf-8 -*-

"""Created on 07.04.2020

.. moduleauthor:: Paweł Pecio
"""

from ._pool import DependencyPool, logger


class ClientsPool(DependencyPool):

    def set_clients(self, clients_settings):
        self._set(clients_settings)

        if 'default' not in self._mods:
            logger.info(f'no default client, you have to explicit give client name everywhere')

    def __call__(self, mod_name: str = 'default'):
        return super().__call__(mod_name)


PatchworkPublishers = ClientsPool()
