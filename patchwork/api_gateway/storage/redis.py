# -*- coding: utf-8 -*-

"""Created on 16.04.2020

.. moduleauthor:: Paweł Pecio
"""
import logging
from typing import Optional

import aioredis
from aioredis import Redis
from pydantic import RedisDsn

from storage.base import Storage


class RedisStorage(Storage):

    class Config(Storage.Config):
        redis_dsn: RedisDsn
        prefix: str = ""

    _redis: Redis

    async def run(self):
        try:
            self._redis: Redis = await aioredis.create_redis_pool(self.settings.redis_dsn)
        except ConnectionRefusedError:
            self.logger.error(f"can't connect to Redis on '{self.settings.redis_dsn}'. "
                         f"Is Redis server running and reachable?")

    async def terminate(self):
        if not hasattr(self, '_redis'):
            self.logger.warning("can't stop Redis storage, missing redis connection")
            return

        self._redis.close()
        await self._redis.wait_closed()
        del self._redis

    async def _set(self, key: str, data: bytes, ttl: Optional[int]):
        await self._redis.set(f"{self.settings.prefix}{key}", data, expire=ttl)

    async def _get(self, key: str) -> bytes:
        return await self._redis.get(f"{self.settings.prefix}{key}")

    async def _exists(self, key: str) -> bool:
        return await self._redis.exists(f"{self.settings.prefix}{key}")

    async def _remove(self, key: str) -> bool:
        return (await self._redis.delete(f"{self.settings.prefix}{key}")) > 0
