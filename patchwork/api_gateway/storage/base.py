# -*- coding: utf-8 -*-

"""Created on 16.04.2020

.. moduleauthor:: Paweł Pecio
"""
import uuid
from patchwork.core import Component
from datetime import timedelta, datetime
from typing import Any, Optional

from patchwork.core.config.base import Dependency


class Storage(Component):
    """
    Base implementation of simple KV storage for API gateway. It allows to setup serialization and client
    side encryption.

    Set serializer as
    """

    class Config(Component.Config):
        serializer: Dependency = Dependency(engine='patchwork.core.serializers:JSONSerializer')
        encryptor: Dependency = Dependency(engine='storage.encryptor:PlainEncryptor')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.serializer = self.settings.serializer.instantiate()
        self.encryptor = self.settings.encryptor.instantiate()

    async def store(self, data: Any, key: str = None, ttl: int = None) -> str:
        """
        Stores given PLAIN data at key. This class or backend must encrypt credentials.
        :param data:
        :param key: optional key, if not given it will be generated
        :param ttl: number of seconds
        :return: key at which data were written, if key is given it always the same as given
        """
        if key is None:
            key = await self.generate_id()
        raw_data = self.serializer.dumps(data)
        await self._set(key, self.encryptor.encrypt(raw_data), ttl=ttl)
        return key

    async def get(self, key: str) -> Any:
        """
        Gets data from given key. Returns as PLAIN. This class or backend must decrypt credentials.
        :raises LookupError: if key does not exist
        :param key:
        :return:
        """
        encrypted = await self._get(key)
        return self.serializer.loads(
            self.encryptor.decrypt(encrypted)
        )

    async def exists(self, key: str) -> bool:
        """
        Returns if given key exists
        :param key:
        :return:
        """
        return await self._exists(key)

    async def remove(self, key: str) -> bool:
        """
        Removes given key
        :param key:
        :return: True if key was removed, False if key was not removed because does not exist
        """
        return await self._remove(key)

    async def generate_id(self) -> str:
        return str(uuid.uuid4())

    async def _get(self, key: str) -> bytes:
        """
        Implement this method to perform actual data fetching.
        :raises LookupError: if key does not exist
        :param key:
        :return:
        """
        raise NotImplementedError()

    async def _set(self, key: str, data: bytes, ttl: Optional[int]):
        """
        Implement this method to perform actual data setting
        :param key:
        :param data:
        :param ttl:
        :return:
        """
        raise NotImplementedError()

    async def _exists(self, key: str) -> bool:
        """
        Implement this method to perform actual existence check
        :param key:
        :return:
        """
        raise NotImplementedError()

    async def _remove(self, key: str) -> bool:
        """
        Implement this method to perform actual removal
        :param key:
        :return: True if key was removed, False if key was not removed because does not exist
        """
        raise NotImplementedError()


class LocMemStorage(Storage):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self._data = {}

    async def _get(self, key: str) -> bytes:
        expire, data = self._data[key]
        if expire is not None and expire < datetime.utcnow():
            await self._remove(key)
            raise LookupError(key)

        return data

    async def _set(self, key: str, data: bytes, ttl: Optional[int]):
        if ttl is not None:
            expire = datetime.utcnow() + timedelta(seconds=ttl)
        else:
            expire = None

        self._data[key] = (expire, data)

    async def _exists(self, key: str) -> bool:
        if key not in self._data:
            return False

        expire, _ = self._data[key]
        if expire is not None and expire < datetime.utcnow():
            await self._remove(key)
            return False

    async def _remove(self, key: str) -> bool:
        if key not in self._data:
            return False

        self._data.pop(key)
