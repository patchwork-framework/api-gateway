# -*- coding: utf-8 -*-
from tortoise import Model, fields


class TortoiseKVModel(Model):

    key = fields.CharField(max_length=128, pk=True)
    value = fields.BinaryField()
    expires = fields.DatetimeField(null=True)
