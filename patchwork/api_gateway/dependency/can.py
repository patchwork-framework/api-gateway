# -*- coding: utf-8 -*-

"""Created on 07.04.2020

.. moduleauthor:: Paweł Pecio
"""


class Permission:
    pass


class Can:
    # dependency for permissions
    pass

    #  Can(edit('users') | view | is_admin)
