# -*- coding: utf-8 -*-

"""Created on 07.04.2020

.. moduleauthor:: Paweł Pecio

Credits:
    Code taken from OPEN PR to Starlette framework (Add pluggable session backends #499),
    original author is alex-oleshkevich
"""

import typing
from collections import defaultdict

from time import time

from starlette.datastructures import MutableHeaders
from starlette.requests import HTTPConnection
from starlette.types import ASGIApp, Message, Receive, Scope, Send

from storage import Storage


class SessionNotLoaded(Exception):
    pass


class SessionMiddleware:
    def __init__(
        self,
        app: ASGIApp,
        backend: Storage,
        session_cookie: str = "session",
        max_age: int = 14 * 24 * 60 * 60,  # 14 days, in seconds
        same_site: str = "lax",
        https_only: bool = False
    ) -> None:
        self.app = app
        self.backend = backend
        self.session_cookie = session_cookie
        self.max_age = max_age
        self.security_flags = "httponly; samesite=" + same_site
        if https_only:  # Secure flag can be used with HTTPS only
            self.security_flags += "; secure"

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        if scope["type"] not in ("http", "websocket"):  # pragma: no cover
            await self.app(scope, receive, send)
            return

        connection = HTTPConnection(scope)
        session_id = connection.cookies.get(self.session_cookie, None)

        scope["session"] = Session(self.backend, session_id, max_age=self.max_age)

        async def send_wrapper(message: Message) -> None:
            if message["type"] == "http.response.start":
                if scope["session"]:
                    # We have session data to persist (data was changed, cleared, etc).
                    nonlocal session_id
                    session_id = await scope["session"].persist()

                    headers = MutableHeaders(scope=message)
                    header_value = "%s=%s; path=/; Max-Age=%d; %s" % (
                        self.session_cookie,
                        session_id,
                        self.max_age,
                        self.security_flags,
                    )
                    headers.append("Set-Cookie", header_value)
                elif scope["session"].is_loaded and scope["session"].is_empty:
                    # no interactions to session were done
                    headers = MutableHeaders(scope=message)
                    header_value = "%s=%s; %s" % (
                        self.session_cookie,
                        "null; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT;",
                        self.security_flags,
                    )
                    headers.append("Set-Cookie", header_value)
            await send(message)

        await self.app(scope, receive, send_wrapper)


class AuthData:

    def __init__(self):
        self._is_authenticated = False
        self._expire = None

        self._backends = {}

    def add(self, backend_id, expire: float = None):
        self._backends[backend_id] = {
            'expire': expire,
            'authenticated': time()
        }
        self._update_authinfo()

    def remove(self, backend_id):
        self._backends.pop(backend_id, None)
        self._update_authinfo()

    def _update_authinfo(self):
        from gateway import settings

        new_state = True
        summary_expire = None

        for auth_step in settings.required_auth:
            if isinstance(auth_step, str):
                auth_step = [auth_step, ]

            step_state = False
            # go through all backends in the step, one of it must be authenticated to consider
            # whole step as done
            for backend in auth_step:

                if backend not in self._backends:
                    # given backend is not on authenticated backends list
                    continue

                # check if authentication has expired
                expire = self._backends[backend]['expire']
                if expire is not None and time() > expire:
                    # authentication expired
                    self._backends.pop(backend)
                    continue

                # backend authenticated and not expired
                # summary expires on earlier backend expiration
                summary_expire = expire if summary_expire is None else min(summary_expire, expire)
                step_state = True

                # authenticated backend found, no need to check rest of backends in this step
                break

            new_state = new_state and step_state

        self._expire = summary_expire
        self._is_authenticated = new_state

    @property
    def is_authenticated(self):
        if self._expire is not None and self._expire < time():
            # summary expired
            self._update_authinfo()

        return self._is_authenticated

    def __getstate__(self):
        state = self.__dict__.copy()
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)


class Session:

    def __init__(self, backend: Storage, session_id: str = None, max_age: int = None) -> None:
        self.session_id = session_id
        self.max_age = max_age
        self._backend = backend
        self.is_loaded = False

        self._data: typing.Dict[str, typing.Any] = {}
        self._auth: typing.Dict[str, AuthData] = defaultdict(AuthData)
        self._current_id = None

    @property
    def is_empty(self) -> bool:
        """Check if session has data."""
        return not bool(self._data) and not bool(self._auth)

    @property
    def data(self) -> typing.Dict:
        if not self.is_loaded:
            raise SessionNotLoaded("Session is not loaded.")
        return self._data

    @data.setter
    def data(self, value: typing.Dict[str, typing.Any]) -> None:
        self._data = value

    @property
    def auth(self) -> typing.Dict:
        if not self.is_loaded:
            raise SessionNotLoaded("Session is not loaded.")
        return self._auth

    @auth.setter
    def auth(self, value: typing.Dict[str, typing.Any]) -> None:
        self._auth = value

    @property
    def current_userid(self):
        return self.data.get('userid')

    async def load(self) -> None:
        """Load data from the backend.
        Subsequent calls do not take any effect."""
        if self.is_loaded:
            return

        if not self.session_id:
            self.data = {}
            self.auth = {}
            self._current_id = None
        else:
            raw = await self._backend.get(self.session_id)
            self.data = raw["data"]
            self.auth = raw["auth"]

        self.is_loaded = True

    async def persist(self) -> str:
        self.session_id = await self._backend.store({
            "data": self._data,
            "auth": self._auth,
        }, self.session_id, ttl=self.max_age)
        return self.session_id

    async def delete(self) -> None:
        if self.session_id:
            self.data = {}
            self.auth = {}
            await self._backend.remove(self.session_id)

    async def regenerate_id(self) -> str:
        self.session_id = await self._backend.generate_id()
        return self.session_id

    def reset_authentication(self):
        """
        Reset all authentication data related to this session.
        Effectively, sing out all users authenticated during this session.
        :return:
        """
        self.auth = defaultdict(AuthData)

    def set_authenticated(self, userid, backend_id, expire: float = None):
        """
        Set that given user has been successfully authorized using the backend.
        Optional expire argument tell how long this authentication is valid.
        :param userid:
        :param backend_id:
        :param expire:
        :return:
        """
        self.auth[userid].add(backend_id, expire)

    def unset_authenticated(self, userid, backend_id):
        """
        Unset user as authenticated using given backend.
        :param userid:
        :param backend_id:
        :return:
        """
        self.auth[userid].remove(backend_id)

    def is_authenticated(self, userid: str = None):
        """
        Check if user is fully authenticated.
        :param userid:
        :return:
        """
        if userid is None:
            return False

        return self.auth[userid].is_authenticated

    def list_authenticated(self) -> typing.List[str]:
        """
        Returns list of all fully authenticated userid's in the session.
        :return:
        """
        return [userid for userid, auth in self.auth if auth.is_authenticated]
